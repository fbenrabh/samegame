# Samegame
Ce projet est réalisé par :
* Farouk Benrabh
* Sami Abdellah

Encadré par :
* Fabien Teytaud
## But du projet
Comparaisons d’IA sur un problème combinatoire simple.
### Backlog
- Création du jeu Samegame.
- Implentation d'un algorithme Random.
- Implentation de l'algorithme Nested Monte-Carlo.
- Implentation de l'algorithme Nested Rollout Policy Adaptation.
- Réalisation d'un script qui permet obtenation les resultas de 100 exécutions d'un algorithme.

## Comment lancer le projet

### Cloner le projet

```
git clone https://gitlab.com/fbenrabh/samegame.git
cd Samegame
cd cpp
```
### Lancer main du projet
```
mkdir build
cd build 
cmake ..
make 
./samegame-cli
```
### génerer la documentation du projet

```
doxygen Doxyfile
```

## Resultat obtenus 

| Algorithme | Nb Runs | Moyenne | Max | Min |
| -----------| ------- | ------- | --- | --- |
| Random     | 100     | 150     | 492 | 152 |
| NMC 1      | 100     | 298,94     | 602 | 188 |
| NMC 2      | 100     |  395.19 | 622 | 214 |
| NRPA       | 100     |   334,87     |  462   |  226   |

## Référence 
https://hal.inria.fr/hal-01406479/document

http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.720.2266&rep=rep1&type=pdf



