#include <deque>
#include <unordered_map>

#ifndef Player_HPP
#define Player_HPP

typedef std::vector<std::vector<int>> Moves;

typedef std::deque<std::vector<int>> PlayoutsSeq;

struct score_playouts {
     int score =0 ;
     PlayoutsSeq _playouts;       
    };
    
    
void test();

int players(Board board);

int random(Board board);

int playNMC(Board board , int level);

int nrpa(Board board);




score_playouts monte_carlo(Board  board);
score_playouts NMC(Board  board,int level);



score_playouts NRPA(Board _board ,std::unordered_map<std::string,double> & _policy, int level);

std::unordered_map<std::string,double> & adapt(Board root,std::unordered_map<std::string,double> & policy_p,PlayoutsSeq playout);

score_playouts monte_carlo_policy(Board board,std::unordered_map<std::string,double> & _policy);

std::string policyKey(Board board ,Coordinate move);
#endif

