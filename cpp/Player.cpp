#include <iostream>	
#include <vector>
#include <string>

#include <random>
#include <deque>

#include "board.hpp"
#include "Samegame.hpp"
#include "Player.hpp"
#include <unordered_map>
#include <ctime>
#include <cstdlib>

Coordinate parse_move(std::string &input);

/**
 * jouer une partie à parire des entées du clavier
 *
 * @param board table de jeu .
 * @return le score de la partie.
 */
int players(Board board){
	
	 int score = 0, result;
    print_board(board, score);
    do {
        std::string input;
        std::cin >> input;
        Coordinate move = parse_move(input);
        result = make_move(board, move);
        if(result == 0) {
            std::cerr << "mouvement non valide!" << std::endl;
            continue;
        }
        score =score+ result;
        print_board(board, score);
    } while (has_valid_move(board));
	return score;
	}


/**
 * jouer une partie aléatoirement
 *
 * @param board table de jeu .
 * @return le score de la partie.
 */
 
int random(Board board){
	 int score = 0, result; 
	  do { 
	 std::vector<Coordinate> moves=valid_move(board); 
	  int randomindex= rand() %moves.size();
        Coordinate move = moves[randomindex];
        result = make_move(board, move);
       
        score += result;
        print_board(board, score);
    } while (has_valid_move(board));
	return score;
	}

/**
 * jouer une partie en utilisant l algorithme NESTED MONTE CARLO
 *
 * @param board table de jeu .
 * @param level de  l algorithme NESTED MONTE CARLO .
 * @return le score de la partie.
 */
 
int playNMC(Board board , int level){
    score_playouts sp = NMC(board,level);
  
     print_board(board,0);
     std::vector<int> xy; 
     int i =0; 
    while(!sp._playouts.empty()){
        xy = sp._playouts.front();
        i +=make_move(board,{(char)xy[0],(char)xy[1]});
        sp._playouts.pop_front();
        print_board(board,i); 
      
        }
        
        return i; 
    }
 

// to play NRPA     
int nrpa(Board board){
	int level =1;
	std::unordered_map<std::string,double> policy;
    score_playouts sp = NRPA(board,policy,level);
     print_board(board,0);
     std::vector<int> xy; 
     int i =0; 
    while(!sp._playouts.empty()){
        xy = sp._playouts.front();
        i +=make_move(board,{(char)xy[0],(char)xy[1]});
        sp._playouts.pop_front();
        print_board(board,i); 
        }
        return i; 
	}





score_playouts monte_carlo(Board board){
        score_playouts sp;     
        PlayoutsSeq playouts;  
        Moves valid_moves = get_valid_moves(board);
        // end game randomly 
            while(has_valid_move(board)){
               std::mt19937 gen(std::random_device{}());
               std::uniform_real_distribution<> dis(0, valid_moves.size());
               int i = dis(gen);
               playouts.push_back(valid_moves[i]);
               sp.score += make_move(board, {(char)valid_moves[i][0],(char)valid_moves[i][1]});
               valid_moves = get_valid_moves(board);
        }
       sp._playouts = playouts;
       return sp;    
}

score_playouts NMC(Board _board , int level){
    // score_playouts => (score , listMoves)
    score_playouts scoreMaxPlayouts;
    score_playouts scoreBestPlayouts;
    scoreBestPlayouts.score = -1;
    int tmpMaxScore =-1;
    int moveScore = 0;    
    // list of moves 
    PlayoutsSeq playouts; 
    PlayoutsSeq p_tmp;
    Moves valid_moves; 

    while(has_valid_move(_board)){
       valid_moves = get_valid_moves(_board);
       tmpMaxScore =-1;
       
       for(int i=0;i<valid_moves.size();i++){
          if(level ==1){
                scoreMaxPlayouts = monte_carlo(make_move_c(_board,valid_moves[i],moveScore));
          }else{
                scoreMaxPlayouts = NMC(make_move_c(_board,valid_moves[i],moveScore),level-1);
          }
          // get max decision
          if((scoreMaxPlayouts.score + moveScore) > tmpMaxScore){
                tmpMaxScore = scoreMaxPlayouts.score + moveScore; 
                p_tmp = scoreMaxPlayouts._playouts;
                p_tmp.push_front(valid_moves[i]);            
          }
          moveScore = 0;           
      }
      // update best score
      if(tmpMaxScore > scoreBestPlayouts.score){
          scoreBestPlayouts.score = tmpMaxScore;
          scoreBestPlayouts._playouts.clear();
          while(!p_tmp.empty()){
              scoreBestPlayouts._playouts.push_back(p_tmp.front());
              p_tmp.pop_front();
             }
        }
      // play the best move
      if(!scoreBestPlayouts._playouts.empty()){
         Moves move(1);
         move[0] = scoreBestPlayouts._playouts.front();
         playouts.push_back(move[0]);
         _board =make_move_c(_board,move[0],moveScore);
         scoreBestPlayouts._playouts.pop_front();
       }
    }
    // best moves sequence
    scoreBestPlayouts._playouts = playouts;
    return scoreBestPlayouts;
}

//**********************************************


// key (simple concatination de position et decistion) pour hash table
std::string policyKey(Board board ,Coordinate move){
	    std::string key="";
	    char tmp ;
	    int t;
	    for (const std::vector<char> &row : board) {
        for (const char &field : row) {
            t = field ;
            tmp = t + '0';
            if(field ==EMPTY)
            tmp = 'e';
            key = key + tmp;
        }
    }
    t= board[move.i][move.j];
    tmp =t + '0';
    int x = move.i;
    int y = move.j;
    char c1 = x+'0';
    char c2 = y+'0';
    key = key +"_"+tmp+"." +c1+"."+c2;
	return key;
}

//******************
 
score_playouts monte_carlo_policy(Board board,std::unordered_map<std::string,double> & _policy){
        score_playouts sp;     
        PlayoutsSeq playouts;  
        Moves valid_moves = get_valid_moves(board);
        int i = -1;
        double sum = 0.0;
        double prop ;
        double rnd;
        srand(time(NULL));
            while(has_valid_move(board)){
				valid_moves = get_valid_moves(board);
				for(int j=0;j<valid_moves.size();j++){
					Coordinate c {valid_moves[j][0],valid_moves[j][1]}; 
					sum += exp(_policy[policyKey(board ,c)]);
				}
                rnd =  (float) rand()/RAND_MAX;
				prop = 0.0;
               for(int j=0;j<valid_moves.size();j++){
				  	Coordinate c {valid_moves[j][0],valid_moves[j][1]}; 
					prop += exp(_policy[policyKey(board ,c)]) / sum;
				    if( prop >= rnd){ 
					    i = j;
					    break;
					}
			   }
			   sum = 0.0;
               playouts.push_back(valid_moves[i]);
               sp.score += make_move(board, {(char)valid_moves[i][0],(char)valid_moves[i][1]});
               valid_moves = get_valid_moves(board);
               i = -1;
        }
       sp._playouts = playouts;
       return sp;    
}


score_playouts NRPA(Board _board ,std::unordered_map<std::string,double>  & _policy, int level){
    score_playouts scoreBestPlayouts;
    score_playouts sp;
    PlayoutsSeq playouts; 
    PlayoutsSeq p_tmp;
    PlayoutsSeq best_playout;
    Moves valid_moves;
    Board root = _board; 
    auto policy = _policy;
    int N =100;
    int best_score =-1;
    if(level ==0){
         return monte_carlo_policy(_board, _policy);
    }else{
        best_score = -1;
        for(int i=0;i<N;i++){
            sp = NRPA(_board,_policy,level-1);
            if(sp.score >= best_score){
              best_score = sp.score;
              best_playout = sp._playouts;
            }
            _policy = adapt(root,_policy,best_playout);
        }
    }
    scoreBestPlayouts.score = best_score;
    scoreBestPlayouts._playouts = best_playout;
    return scoreBestPlayouts;
}


std::unordered_map<std::string,double> & adapt(Board root,std::unordered_map<std::string,double>  & policy_p,PlayoutsSeq playout){
  Board position = root;
  std::vector<int> decision;
  double alpha =0.6;
  for(int i=0;i<playout.size();i++){
    decision = playout[i];
    double z = 0;
    for(int j=0;j< get_valid_moves(position).size();j++){
		Coordinate c {get_valid_moves(position)[j][0],get_valid_moves(position)[j][1]}; 
    z+=  exp(policy_p[policyKey(position ,c)]);
    }
    Moves valid_moves = get_valid_moves(position);
    
    for(int j=0;j<valid_moves.size();j++){
		Coordinate c {valid_moves[j][0],valid_moves[j][1]};	
		if(valid_moves[j][0] == decision[0] && valid_moves[j][1] == decision[1]){ 
		  policy_p[policyKey(position ,c)] = policy_p[policyKey(position ,c)] + alpha;
		}else{
		policy_p[policyKey(position ,c)] = policy_p[policyKey(position ,c)]-alpha *exp(policy_p[policyKey(position ,c)])/z;   
		}  
    }
    make_move(position, {(char)decision[0],(char)decision[1]});
  }
  return policy_p;    
}

//******

Coordinate parse_move(std::string &input) {
    if(input[0] >= 97) {
        return {(char)(input[1] - 48), (char)(input[0] - 97)};
    }
    return {(char)(input[1] - 48), (char)(input[0] - 65)};
}
