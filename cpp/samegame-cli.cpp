using namespace std;
#include <iostream>
#include <vector>
#include <string>
#include "board.hpp"
#include "Samegame.hpp"
#include "Player.hpp"
#include <deque>


int main() {
	int choix;
    int score = 0, result;
    Board board(9, std::vector<char>(9));
    generate_board(board);
    
    do{
	 
    std::clog << "entrez votre choix :" << std::endl;
    std::clog << "1 ==> Player :" << std::endl;
    std::clog << "2 ==> Random  :" << std::endl;
    std::clog << "3 ==> Nested MonteCarlo  :" << std::endl;
    std::clog << "4 ==> Nested Rollout Policy Adaption  :" << std::endl;
    std::clog << "0 ==> exit  :" << std::endl;
    std::cin >> choix;
        
    switch(choix) {
    case 1 : 
    score=players(board);
    std::cout << "Score player : " << score << std::endl;
    break;
    case 2 : 
    score=random(board);
    std::cout << "Score random : " << score << std::endl;
    break;
    case 3 :
    int level;
    std::clog << "entrez le level  :" << std::endl;
    std::cin >> level;
    
   score=playNMC(board,level);
   std::cout << "Score nmc : " << score << std::endl;
   // playNMC(board,2);
      
    break;
    case 4 :
    score=nrpa(board);
    std::cout << "Score nrpa : " << score << std::endl;
    break;
    
}
 std::clog << "*******************************************************************" << std::endl;
}while(choix!=0) ;
    return 0;
}




