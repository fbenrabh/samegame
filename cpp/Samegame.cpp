#include <algorithm>
#include "board.hpp"
#include "Samegame.hpp"

bool has_neighbors(const Board &board, Coordinate c);

void find_adjacent(const Board &board, char i, char j, std::vector<Coordinate> &list);

bool is_in_list(const std::vector<Coordinate> &list, Coordinate value);

void remove_blocks(Board &board, std::vector<Coordinate> &list);

void block_drop(Board &board);

void column_constrict(Board &board);

bool column_empty(const Board &board, int j);


/**
 * vérifie si on peut jouer d'autres coups.
 *
 * @param board table de jeu .
 * @return vrai si si on peut jouer d'autres coups,faux si on la partie est términé.
 */
bool has_valid_move(const Board &board) {
    for (char i = 0; i < 9; i++) {
        for (char j = 0; j < 9; j++) {
            if (has_neighbors(board, {i, j})) {
                return true;
            }
        }
    }
    return false;
}

/**
 * retoune les cordinations des coups possibles.
 *
 * @param board table de jeu .
 * @return vecteur qui contient les cordinations des coups possbles.
 */
 std::vector<Coordinate>  valid_move(const Board &board) {
	 std::vector<Coordinate> cord;

     for (char i = 0; i < 9; i++) {
        for (char j = 0; j < 9; j++) {
            if (has_neighbors(board, {i, j})) {
                 cord.push_back({char(i), char(j)});
            }
        }
    }
    return cord;
}

/**
 * réalise un coup sur le table de jeu.
 *
 * @param board table de jeu .
 * @param c les cordonées du coup à jouer .
 * @return le score du coup .
 */
int make_move(Board &board, Coordinate c) {
    if (c.i > 8 || c.j > 8 || c.i < 0 || c.j < 0) {
        return 0;
    }
    if (board[c.i][c.j] == EMPTY || !has_neighbors(board, c)) {
        return 0;
    }
    std::vector<Coordinate> list;
    find_adjacent(board, c.i, c.j, list);
    remove_blocks(board, list);
    block_drop(board);
    column_constrict(board);
    int n = list.size();
    return n * (n - 1);
}
/**
 * réalise un coup sur une copie de table de jeu.
 *
 * @param board table de jeu .
 * @param moves les cordonées du coup à jouer .
 * @return board apres le coup .
 */
Board make_move_c(Board board, std::vector<int> move , int &score) {
    Coordinate c {(char)move[0],(char)move[1]};
    if (c.i > 8 || c.j > 8 || c.i < 0 || c.j < 0) {
        return board;
    }
    if (board[c.i][c.j] == EMPTY || !has_neighbors(board, c)) {
        return board;
    }
    std::vector<Coordinate> list;
    find_adjacent(board, c.i, c.j, list);
    remove_blocks(board, list);
    block_drop(board);
    column_constrict(board);
    int n = list.size();
    score =  n * (n - 1);
    return board;
}

/**
 * permet de savoir les coups possibles.
 *
 * @param board table de jeu .
 * @return les coups possibles .
 */
Moves get_valid_moves(const Board &board) {
    Moves list ;
        for (char i = 0; i < 9; i++) {
          for (char j = 0; j < 9; j++) {
            if (has_neighbors(board, {i, j})) {
                list.push_back({i , j});
            }
        }
    }    
    return list;
}

/**
 * vérifier s'il y a des cases adjacentes dans le tableau de jeu .
 *
 * @param board table de jeu .
 * @param c coordonées à vérifier.
 * @return vrai s'il y a des cases adjacentes dans le tableau de jeu pour les coordonées passées en paramétre sinon faux.
 */
bool has_neighbors(const Board &board, Coordinate c) {
    int i = c.i;
    int j = c.j;
    if (board[i][j] == EMPTY) {
        return false;
    }
    return (i < 8 && board[i + 1][j] == board[i][j]) || (j < 8 && board[i][j + 1] == board[i][j]) ||
           (i > 0 && board[i - 1][j] == board[i][j]) || (j > 0 && board[i][j - 1] == board[i][j]);
}

/**
 * vérifier s'il y a des cases adjacentes dans le tableau de jeu .
 *
 * @param board table de jeu .
 * @param i coordonées à vérifier.
 * @param j coordonées à vérifier.
 * @param c coordonées à vérifier.
 * @return vrai si les  coordonées entree en parmetre sont adjacentes sinon faux.
 */
void find_adjacent(const Board &board, char i, char j, std::vector<Coordinate> &list) {
    if (is_in_list(list, {i, j})) {
        return;
    }
    list.push_back({i, j});
    if (i < 8 && board[i + 1][j] == board[i][j]) {
        find_adjacent(board, i + 1, j, list);
    }
    if (j < 8 && board[i][j + 1] == board[i][j]) {
        find_adjacent(board, i, j + 1, list);
    }
    if (i > 0 && board[i - 1][j] == board[i][j]) {
        find_adjacent(board, i - 1, j, list);
    }
    if (j > 0 && board[i][j - 1] == board[i][j]) {
        find_adjacent(board, i, j - 1, list);
    }
}

/**
 * vérifier la cases existe dans un vecteur .
 *
 * @param list vecteur des coordonées.
 * @param c coordonées à vérifier.
 * @return vrai si la cases existe dans le vecteur list sinon faux.
 */
bool is_in_list(const std::vector<Coordinate> &list, Coordinate value) {
    for (const Coordinate &c : list) {
        if (c.i == value.i && c.j == value.j) {
            return true;
        }
    }
    return false;
}


void remove_blocks(Board &board, std::vector<Coordinate> &list) {
    for (const Coordinate &c : list) {
        board[c.i][c.j] = EMPTY;
    }
}


void block_drop(Board &board) {
    for (int j = 0; j < 9; j++) {
        for (int i = 8; i > 0; i--) {
            int k = i;
            while (k <= 8 && board[k][j] == EMPTY && board[k - 1][j] != EMPTY) {
                std::swap(board[k][j], board[k - 1][j]);
                k++;
            }
        }
    }
}
/**
 * generer le tableau du jeu 
 *
 * @param Board le tableau de jeu.
 *
 */
void column_constrict(Board &board) {
    for (int j = 1; j < 9; j++) {
        int k = j;
        while (!column_empty(board, k) && column_empty(board, k - 1) && k >= 1) {
            for (int i = 0; i < 9; i++) {
                std::swap(board[i][k], board[i][k - 1]);
            }
            k--;
        }
    }
}

/**
 * vérifier la cases est vide .
 *
 * @param Board le tableau de jeu.
 * @param j la colonne a vérifier.
 * @return vrai si la cases n'est pas vide ,faux si la case est vide.
 */
bool column_empty(const Board &board, int j) {
    for (int i = 0; i < 9; i++) {
        if (board[i][j] != EMPTY) {
            return false;
        }
    }
    return true;
}

