#ifndef SAMEGAME_BOARD_H
#define SAMEGAME_BOARD_H

#include <vector>
char const RED = 31;
char const GREEN = 32;
char const YELLOW = 33;
char const BLUE = 34;
char const MAGENTA = 35;
char const EMPTY = 0;


struct Coordinate {
    char i;
    char j;
};

typedef std::vector<std::vector<char>> Board;

const char color_txt[6] = {'R','G','Y','B','M',' '};

int map_color_text(char c);

void generate_board(Board &board);

void print_board(const Board &board, int score);


#endif //SAMEGAME_BOARD_H
