var searchData=
[
  ['magenta_24',['MAGENTA',['../board_8hpp.html#a4d999f1457b8e3a220fd0cae0c439683',1,'board.hpp']]],
  ['main_25',['main',['../samegame-cli_8cpp.html#ae66f6b31b5ad750f1fe042a706a4e3d4',1,'main():&#160;samegame-cli.cpp'],['../samegame-test_8cpp.html#a3c04138a5bfe5d72780bb7e82a18e627',1,'main(int argc, char **argv):&#160;samegame-test.cpp']]],
  ['make_5fmove_26',['make_move',['../Samegame_8cpp.html#ad6f62ad2e15d66c9ef6687e686410d20',1,'make_move(Board &amp;board, Coordinate c):&#160;Samegame.cpp'],['../Samegame_8hpp.html#ad6f62ad2e15d66c9ef6687e686410d20',1,'make_move(Board &amp;board, Coordinate c):&#160;Samegame.cpp']]],
  ['make_5fmove_5fc_27',['make_move_c',['../Samegame_8cpp.html#ab0c0b2c309d8319d575b85e909b6c62a',1,'make_move_c(Board board, std::vector&lt; int &gt; move, int &amp;score):&#160;Samegame.cpp'],['../Samegame_8hpp.html#ab0c0b2c309d8319d575b85e909b6c62a',1,'make_move_c(Board board, std::vector&lt; int &gt; move, int &amp;score):&#160;Samegame.cpp']]],
  ['map_5fcolor_28',['map_color',['../board_8cpp.html#a6f482cba42820cf0c5a45c6dd49c2781',1,'board.cpp']]],
  ['map_5fcolor_5ftext_29',['map_color_text',['../board_8cpp.html#ad03e93229ade64f7e73c99caa1b728b0',1,'map_color_text(char c):&#160;board.cpp'],['../board_8hpp.html#ad03e93229ade64f7e73c99caa1b728b0',1,'map_color_text(char c):&#160;board.cpp']]],
  ['monte_5fcarlo_30',['monte_carlo',['../Player_8cpp.html#a7129564b64646615c7d50654c704098a',1,'monte_carlo(Board board):&#160;Player.cpp'],['../Player_8hpp.html#a7129564b64646615c7d50654c704098a',1,'monte_carlo(Board board):&#160;Player.cpp']]],
  ['monte_5fcarlo_5fpolicy_31',['monte_carlo_policy',['../Player_8cpp.html#ae2d82026466f7092c462925de991c9bd',1,'Player.cpp']]],
  ['moves_32',['Moves',['../Player_8hpp.html#a7f4e0ced5b8f40dee723e8aa7251a085',1,'Moves():&#160;Player.hpp'],['../Samegame_8hpp.html#a7f4e0ced5b8f40dee723e8aa7251a085',1,'Moves():&#160;Samegame.hpp']]]
];
