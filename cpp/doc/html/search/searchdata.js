var indexSectionsWithContent =
{
  0: "_abcefghijmnprstvy",
  1: "cs",
  2: "bcps",
  3: "abcfghimnprtv",
  4: "_bcegijmrsy",
  5: "bmp"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Classes",
  2: "Fichiers",
  3: "Fonctions",
  4: "Variables",
  5: "Définitions de type"
};

