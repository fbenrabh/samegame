#include <iostream>
#include <random>
#include "board.hpp"

void print_row_num(int i);

void print_color(const char &color);

void reset_color();

char map_color(char c);

/**
 * génerer le tableau de jeu .
 *
 * @param board tableau de jeu .
 * 
 */
void generate_board(Board &board) {
    std::mt19937 gen(std::random_device{}());
    std::uniform_real_distribution<> dis(0, 5);
    for (std::vector<char> &row : board) {
        for (char &field : row) {
            field = map_color((char)dis(gen));
        }
    }

}
/**
 * afficher le tableau de jeu .
 *
 * @param board tableau de jeu .
 * @param score le score de la partie .
 * 
 */
void print_board(const Board &board, int score) {
    std::cout << "Current score: " << score << std::endl;
    std::cout << "  A B C D E F G H I" << std::endl;
    int i = 0;
    for (const std::vector<char> &row : board) {
        print_row_num(i++);
        for (const char &field : row) {
            print_color(field);
        }
        std::cout << std::endl;
    }
    reset_color();
}
/**
 * choisir une couleur .
 *
 * @param i un chiffre déstiner pour une couleur.
 */
void print_row_num(int i) {
    reset_color();
    std::cout << i;
}

void print_color(const char &color) {
	int i = map_color_text(color);
    if (color != EMPTY) {
        printf(" %c[%dm%c", 0x1B, color,color_txt[i]);
    } else {
        std::cout << "  ";
    }
}
/**
 * renitialiser les couleurs.
 * 
 */
void reset_color() {
    printf("%c[0m", 0x1B);
}

/**
 * retourner une couleur .
 *
 * @param c un chiffre déstiner pour une couleur.
 * @return la couleur destiné au chiffre entrer en paramétre
 */
char map_color(char c) {
    switch (c) {
        case 0:
            return RED;
        case 1:
            return GREEN;
        case 2:
            return YELLOW;
        case 3:
            return BLUE;
        case 4:
            return MAGENTA;
        default:
            return RED;
    }
}

/**
 * retourner un chiffre déstiner pour une couleur .
 *
 * @param c le nom du couleur.
 * @return un nombre destinée au  couleur choisie 
 */
int map_color_text(char c) {
    switch (c) {
        case RED:
            return 0;
        case GREEN:
            return 1;
        case YELLOW:
            return 2;
        case BLUE:
            return 3;
        case MAGENTA:
            return 4;
        default:
            return 0;
    }
}
