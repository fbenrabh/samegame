#ifndef SAMEGAME_HPP
#define SAMEGAME_HPP


bool has_valid_move(const Board &board);

std::vector<Coordinate>  valid_move(const Board &board);

int make_move(Board &board, Coordinate c);

typedef std::vector<std::vector<int>> Moves;
Moves  get_valid_moves(const Board &board);
Board make_move_c(Board board, std::vector<int> move , int & score);

#endif

