{ pkgs ? import <nixpkgs> {} }:

with pkgs;

stdenv.mkDerivation {
  name = "samegame";
  src = ./.;
  buildInputs = [
    cmake
    pkgconfig
    doxygen
    cpputest
  ];
}

